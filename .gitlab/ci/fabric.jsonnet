local job_build(minecraft_version, fabric_version) = {
  image: "quay.io/buildah/stable:latest",
  stage: "build",
  variables: {
    "BUILDAH_FORMAT": "oci",
    "BUILDAH_LAYERS": "true",
    "CONTAINER_TEST_IMAGE": "$CI_REGISTRY_IMAGE:ci-$CI_PIPELINE_ID-fabric-" + minecraft_version + "-" + fabric_version,
    "CONTAINER_RELEASE_IMAGE": "$CI_REGISTRY_IMAGE:fabric-" + minecraft_version + "-" + fabric_version,
    "MINECRAFT_VERSION": minecraft_version,
    "FABRIC_VERSION": fabric_version,
  },
  before_script: ["echo \"$CI_REGISTRY_PASSWORD\" | buildah login -u \"$CI_REGISTRY_USER\" --password-stdin \"$CI_REGISTRY\""],
  script: [
    "buildah build --cache-from \"$CI_REGISTRY_IMAGE/cache\" --cache-to \"$CI_REGISTRY_IMAGE/cache\" -t \"$CONTAINER_TEST_IMAGE\" --build-arg MINECRAFT_VERSION=\"$MINECRAFT_VERSION\" --build-arg fabric_version=\"$fabric_version\" --file fabric/Dockerfile --ignorefile fabric/.dockerignore fabric",
    "buildah push \"$CONTAINER_TEST_IMAGE\" \"docker://$CONTAINER_TEST_IMAGE\"",
  ],
};
local job_release(minecraft_version, fabric_version) = {
  image: "quay.io/buildah/stable:latest",
  stage: "release",
  variables: {
    "BUILDAH_FORMAT": "oci",
    "BUILDAH_LAYERS": "true",
    "CONTAINER_TEST_IMAGE": "$CI_REGISTRY_IMAGE:ci-$CI_PIPELINE_ID-fabric-" + minecraft_version + "-" + fabric_version,
    "CONTAINER_RELEASE_IMAGE": "$CI_REGISTRY_IMAGE:fabric-" + minecraft_version + "-" + fabric_version,
    "MINECRAFT_VERSION": minecraft_version,
    "fabric_version": fabric_version,
  },
  before_script: ["echo \"$CI_REGISTRY_PASSWORD\" | buildah login -u \"$CI_REGISTRY_USER\" --password-stdin \"$CI_REGISTRY\""],
  script: [
    "buildah pull \"docker://$CONTAINER_TEST_IMAGE\"",
    "buildah tag \"$CONTAINER_TEST_IMAGE\" \"$CONTAINER_RELEASE_IMAGE\"",
    "buildah push \"$CONTAINER_RELEASE_IMAGE\" \"docker://$CONTAINER_RELEASE_IMAGE\"",
  ],
  needs: ["job_build/fabric-" + minecraft_version + "-" + fabric_version],
};
local versions = [
  ["1.20.2", "0.14.23"],
  ["1.20.1", "0.14.23"],
];
{
  stages: ["build", "release"]
}
+
{
  ['job_build/fabric-' + x[0] + '-' + x[1]]: job_build(x[0], x[1]) for x in versions
}
+
{
  ['job_release/fabric-' + x[0] + '-' + x[1]]: job_release(x[0], x[1]) for x in versions
}
