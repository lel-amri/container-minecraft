local job_build(minecraft_version, forge_version) = {
  image: "quay.io/buildah/stable:latest",
  stage: "build",
  variables: {
    "BUILDAH_FORMAT": "oci",
    "BUILDAH_LAYERS": "true",
    "CONTAINER_TEST_IMAGE": "$CI_REGISTRY_IMAGE:ci-$CI_PIPELINE_ID-forge-" + minecraft_version + "-" + forge_version,
    "CONTAINER_RELEASE_IMAGE": "$CI_REGISTRY_IMAGE:forge-" + minecraft_version + "-" + forge_version,
    "MINECRAFT_VERSION": minecraft_version,
    "FORGE_VERSION": forge_version,
  },
  before_script: ["echo \"$CI_REGISTRY_PASSWORD\" | buildah login -u \"$CI_REGISTRY_USER\" --password-stdin \"$CI_REGISTRY\""],
  script: [
    "buildah build --cache-from \"$CI_REGISTRY_IMAGE/cache\" --cache-to \"$CI_REGISTRY_IMAGE/cache\" -t \"$CONTAINER_TEST_IMAGE\" --build-arg MINECRAFT_VERSION=\"$MINECRAFT_VERSION\" --build-arg FORGE_VERSION=\"$FORGE_VERSION\" --file forge/Dockerfile --ignorefile forge/.dockerignore forge",
    "buildah push \"$CONTAINER_TEST_IMAGE\" \"docker://$CONTAINER_TEST_IMAGE\"",
  ],
};
local job_release(minecraft_version, forge_version) = {
  image: "quay.io/buildah/stable:latest",
  stage: "release",
  variables: {
    "BUILDAH_FORMAT": "oci",
    "BUILDAH_LAYERS": "true",
    "CONTAINER_TEST_IMAGE": "$CI_REGISTRY_IMAGE:ci-$CI_PIPELINE_ID-forge-" + minecraft_version + "-" + forge_version,
    "CONTAINER_RELEASE_IMAGE": "$CI_REGISTRY_IMAGE:forge-" + minecraft_version + "-" + forge_version,
    "MINECRAFT_VERSION": minecraft_version,
    "FORGE_VERSION": forge_version,
  },
  before_script: ["echo \"$CI_REGISTRY_PASSWORD\" | buildah login -u \"$CI_REGISTRY_USER\" --password-stdin \"$CI_REGISTRY\""],
  script: [
    "buildah pull \"docker://$CONTAINER_TEST_IMAGE\"",
    "buildah tag \"$CONTAINER_TEST_IMAGE\" \"$CONTAINER_RELEASE_IMAGE\"",
    "buildah push \"$CONTAINER_RELEASE_IMAGE\" \"docker://$CONTAINER_RELEASE_IMAGE\"",
  ],
  needs: ["job_build/forge-" + minecraft_version + "-" + forge_version],
};
local versions = [
  ["1.20.1", "47.1.47"],
];
{
  stages: ["build", "release"]
}
+
{
  ['job_build/forge-' + x[0] + '-' + x[1]]: job_build(x[0], x[1]) for x in versions
}
+
{
  ['job_release/forge-' + x[0] + '-' + x[1]]: job_release(x[0], x[1]) for x in versions
}
