#!/bin/sh
set -eu
print_usage() {
    printf "Usage: %s: [-t OUTPUT_IMAGE_NAME] MINECRAFT_VERSION FABRIC_VERSION\n" "$0"
}
minecraft_version=
fabric_version=
output_image_name=
while getopts t: name
do
    case "${name}" in
    t)
        output_image_name="${OPTARG}"
        ;;
    ?)
        print_usage >&2
        exit 2
        ;;
    esac
done
shift $(($OPTIND - 1))
if [ "$#" = "0" ] ; then
    print_usage >&2
    exit 2
fi
minecraft_version="$1"
shift
if [ "$#" = "0" ] ; then
    print_usage >&2
    exit 2
fi
fabric_version="$1"
shift
set --
if [ -n "${output_image_name}" ] ; then
    set -- "$@" --tag "${output_image_name}"
fi
if [ -n "${minecraft_version}" ] ; then
    set -- "$@" --build-arg "MINECRAFT_VERSION=${minecraft_version}"
fi
if [ -n "${fabric_version}" ] ; then
    set -- "$@" --build-arg "FABRIC_VERSION=${fabric_version}"
fi
buildah build --format oci --file Dockerfile --ignorefile .dockerignore "$@" .
