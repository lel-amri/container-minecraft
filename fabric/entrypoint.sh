#!/bin/sh
set -eu
quote_value() (
	value="$1"
	shift
	printf "%s" "${value}" | sed -e "s/'/'\\\\''/g; 1s/^/'/; \$s/\$/'/"
)
readonly CWD="$(pwd -P)"
if [ "${AUTO_ACCEPT_MINECRAFT_EULA+yes}" = "yes" ] ; then
    if [ ! -e "eula.txt" ] ; then
        echo "Automatically accepting Minecraft's EULA"
        echo "eula=true" >"${CWD}/eula.txt"
    else
        echo "Warning: Did not automatically accept Minecraft's EULA because ${CWD}/eula.txt already exists. Maybe Minecraft's EULA is already accepted." >&2
    fi
fi
if [ -n "${MINECRAFT_SERVER_PORT:=}" ] ; then
    if [ ! -e "${CWD}/server.properties" ] ; then
        echo "server-port=${MINECRAFT_SERVER_PORT}" >"${CWD}/server.properties"
    fi
fi
set -- "-Dfabric.gameJarPath=/minecraft/server.jar" "-jar" "/minecraft/fabric-server-launch.jar" "$@"
if [ -n "${USER_JVM_ARGS_FILE:-}" ] ; then
    set -- "@${USER_JVM_ARGS_FILE}" "$@"
fi
java "$@"
