# OCI Image for a Minecraft Forge server

## Quick start

Follow these steps:

* Create a volume that will store your server files:
  `docker volume create minecraft-workdir`

* Create a default `server.properties` file:
  `docker run -v minecraft-workdir:/workdir --workdir /workdir --rm -it -e AUTO_ACCEPT_MINECRAFT_EULA=1 registry.gitlab.com/lel-amri/container-minecraft:forge-1.20.1-47.1.79 --initSettings`

* Edit the `server.properties` file:
  `docker run -v minecraft-workdir:/workdir --rm -it registry.gitlab.com/lel-amri/oci-image-toolbox:latest sh -c 'nano /workdir/server.properties'`

* Create an argument file for `java`:
  `docker run -v minecraft-workdir:/workdir --rm registry.gitlab.com/lel-amri/oci-image-toolbox:latest sh -c 'echo "-Xmx4G" >/workdir/user_jvm_args.txt'`

* Start the server: 
  `docker run -v minecraft-workdir:/workdir --workdir /workdir --rm -it --publish 127.0.0.1:25565:25565/tcp -e AUTO_ACCEPT_MINECRAFT_EULA=1 -e USER_JVM_ARGS_FILE=/workdir/user_jvm_args.txt registry.gitlab.com/lel-amri/container-minecraft:forge-1.20.1-47.1.79`

You can now connect to your server on `127.0.0.1:25565`.

## How to use

A very simple run would be: `docker run -it registry.gitlab.com/lel-amri/container-minecraft:forge-1.20.1-47.1.79`

An ephemeral container is recommended, thus you'll have to set a custom working
directory to persist your server files:
`docker run --workdir /workdir -v minecraft-workdir:/workdir --rm -it registry.gitlab.com/lel-amri/container-minecraft:forge-1.20.1-47.1.79`

Environment variables supported :

* **AUTO_ACCEPT_MINECRAFT_EULA**: When set, automatically accepts Minecraft's
  EULA.

* **MINECRAFT_SERVER_PORT**: Sets the port the server should listen to. This
  should be left as is and port mapping should be used instead (with
  `docker run`'s `--publish` option).

* **USER_JVM_ARGS_FILE**: A path to an argument file that will be passed to the
  `java` command. It will be the first `java` arguments.

The command passed to `docker run` is passed as arguments to `java`.
