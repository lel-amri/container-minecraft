#!/bin/sh
set -eu
print_usage() {
    printf "Usage: %s: [-t OUTPUT_IMAGE_NAME] MINECRAFT_VERSION NEOFORGED_VERSION\n" "$0"
}
minecraft_version=
neoforged_version=
output_image_name=
while getopts t: name
do
    case "${name}" in
    t)
        output_image_name="${OPTARG}"
        ;;
    ?)
        print_usage >&2
        exit 2
        ;;
    esac
done
shift $(($OPTIND - 1))
if [ "$#" = "0" ] ; then
    print_usage >&2
    exit 2
fi
minecraft_version="$1"
shift
if [ "$#" = "0" ] ; then
    print_usage >&2
    exit 2
fi
neoforged_version="$1"
shift
set --
if [ -n "${output_image_name}" ] ; then
    set -- "$@" --tag "${output_image_name}"
fi
if [ -n "${minecraft_version}" ] ; then
    set -- "$@" --build-arg "MINECRAFT_VERSION=${minecraft_version}"
fi
if [ -n "${neoforged_version}" ] ; then
    set -- "$@" --build-arg "FORGE_VERSION=${neoforged_version}"
fi
buildah build --format oci --file Dockerfile --ignorefile .dockerignore "$@" .
