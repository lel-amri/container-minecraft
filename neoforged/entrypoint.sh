#!/bin/sh
set -eu
quote_value() (
	value="$1"
	shift
	printf "%s" "${value}" | sed -e "s/'/'\\\\''/g; 1s/^/'/; \$s/\$/'/"
)
readonly CWD="$(pwd -P)"
if [ "${AUTO_ACCEPT_MINECRAFT_EULA+yes}" = "yes" ] ; then
    if [ ! -e "eula.txt" ] ; then
        echo "Automatically accepting Minecraft's EULA"
        echo "eula=true" >"${CWD}/eula.txt"
    else
        echo "Warning: Did not automatically accept Minecraft's EULA because ${CWD}/eula.txt already exists. Maybe Minecraft's EULA is already accepted." >&2
    fi
fi
if [ -n "${MINECRAFT_SERVER_PORT:=}" ] ; then
    if [ ! -e "${CWD}/server.properties" ] ; then
        echo "server-port=${MINECRAFT_SERVER_PORT}" >"${CWD}/server.properties"
    fi
fi
# Path to the original "unix_args.txt" file
readonly original_forgeargs="/minecraft/libraries/net/neoforged/forge/${MINECRAFT_VERSION}-${FORGE_VERSION}/unix_args.txt"
# Path to the modified "unix_args.txt" file
readonly modified_forgeargs="$(mktemp)"
trap "rm -f \"\${modified_forgeargs}\"" 0 2 3 15
awk -v PREFIX=/minecraft \
"
match(\$0, /^(-p|-DlegacyClassPath)( +|=)/) != 0 {
    opt = substr(\$0, RSTART, RLENGTH)
    val = substr(\$0, RSTART + RLENGTH)
    split(val, elems, /:/)
    for (elem_i in elems) {
        elems[elem_i] = sprintf(\"%s/%s\", PREFIX, elems[elem_i])
    }
    val = \"\"
    i = 0
    for (elem_i in elems) {
        if (i > 0) {
            val = sprintf(\"%s:\", val)
        }
        val = sprintf(\"%s%s\", val, elems[elem_i])
        i++
    }
    \$0 = sprintf(\"%s%s\", opt, val)
}
match(\$0, /^(-DlibraryDirectory)( +|=)/) != 0 {
    opt = substr(\$0, RSTART, RLENGTH)
    val = substr(\$0, RSTART + RLENGTH)
    \$0 = sprintf(\"%s%s/%s\", opt, PREFIX, val)
}
{ print }
" \
<"${original_forgeargs}" >"${modified_forgeargs}"
set -- "@${modified_forgeargs}" "$@"
if [ -n "${USER_JVM_ARGS_FILE:-}" ] ; then
    set -- "@${USER_JVM_ARGS_FILE}" "$@"
fi
java "$@"
